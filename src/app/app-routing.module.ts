import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RaceListComponent} from './components/race-list/race-list.component';
import {RaceDetailComponent} from './components/race-detail/race-detail.component';

const routes: Routes = [
    {
        path: '', component: RaceListComponent
    }, {
        path: 'event/:eventId', component: RaceDetailComponent
    },
    {
        path: '**', component: RaceListComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
