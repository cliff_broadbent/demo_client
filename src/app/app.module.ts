import 'hammerjs';

import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {HttpCommsService} from './services/http-comms/http-comms.service';
import {RaceDetailResponseHydratorService} from './services/race-detail-response-hydrator/race-detail-response-hydrator.service';
import {RaceListResponseHydratorService} from './services/race-list-response-hydrator/race-list-response-hydrator.service';
import {LadbrokesApiRpcService} from './services/ladbrokes-api-rpc/ladbrokes-api-rpc.service';
import {ConfigService} from './services/config/config.service';
import {ApplicationStateService} from './models/application-state/application-state.service';
import { RaceListComponent } from './components/race-list/race-list.component';
import { RaceDetailComponent } from './components/race-detail/race-detail.component';
import {
    MdButtonModule, MdCardModule, MdIconModule, MdListModule, MdProgressBarModule, MdProgressSpinnerModule,
    MdToolbarModule
} from "@angular/material";


@NgModule({
    declarations: [
        AppComponent,
        RaceListComponent,
        RaceDetailComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MdToolbarModule,
        MdButtonModule,
        MdListModule,
        MdIconModule,
        MdCardModule,
        MdProgressSpinnerModule,
        MdProgressBarModule,
    ],
    providers: [
        ConfigService,
        ApplicationStateService,
        HttpCommsService,
        RaceDetailResponseHydratorService,
        RaceListResponseHydratorService,
        LadbrokesApiRpcService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
