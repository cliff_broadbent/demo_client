import { TestBed, inject } from '@angular/core/testing';

import { LadbrokesApiRpcService } from './ladbrokes-api-rpc.service';

describe('LadbrokesApiRpcService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LadbrokesApiRpcService]
    });
  });

  it('should ...', inject([LadbrokesApiRpcService], (service: LadbrokesApiRpcService) => {
    expect(service).toBeTruthy();
  }));
});
