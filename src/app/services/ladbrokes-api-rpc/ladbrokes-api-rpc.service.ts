import {Injectable} from '@angular/core';
import {HttpCommsService} from '../http-comms/http-comms.service';
import {RaceListResponseHydratorService} from '../race-list-response-hydrator/race-list-response-hydrator.service';
import {RaceDetailResponseHydratorService} from '../race-detail-response-hydrator/race-detail-response-hydrator.service';
import {ApplicationStateService} from '../../models/application-state/application-state.service';
import {ConfigService} from '../config/config.service';
import {ApiRpc} from './api-rpc';


/**
 * Responsible for RPCs
 */
@Injectable()
export class LadbrokesApiRpcService implements ApiRpc {

    constructor(private configuration: ConfigService,
                private stateModel: ApplicationStateService,
                private httpComms: HttpCommsService,
                private raceListHydrator: RaceListResponseHydratorService,
                private eventDetailHydrator: RaceDetailResponseHydratorService) {
    }


    /**
     * Perform all steps required to retrieve a list of upcoming races and update the App State model
     */
    public updateRaceList() {
        const url = this.generateRaceListUrl();

        console.log('Requested load of Race List ...');

        this.httpComms.httpGet(url, null).then((response) => {

            console.log(' ..... response received from api');

            let parsedRaceList = this.raceListHydrator.hydrateRaceList(response);

            this.stateModel.updateRaceList(parsedRaceList);

        }).catch((ex) => {

            console.log('ERROR - RPC');
            console.log(ex);
            alert('Request to API at "' + this.configuration.network.host + '" has failed.');
        });
    }


    // @todo: this needs to be moved to a URL generator service. Bad form to include implementation here
    private generateRaceListUrl() {

        return this.configuration.network.host + '/ladbrokes/events';
    }


    /**
     * Fetch Details of a specific Race and add it to it's respective Race by Id
     *
     * @param eventId
     */
    public getRaceDetails(eventId: number) {

        console.log('Requested load of Details for eventId ' + eventId + ' ...');

        const url = this.generateEventDetailsUrl(eventId);

        this.httpComms.httpGet(url, null).then((response) => {

            console.log(' ..... response received from api');

            let parsedRaceEventDetails = this.eventDetailHydrator.hydrateRaceDetails(response);

            this.stateModel.updateRaceEventDetails(eventId, parsedRaceEventDetails);

        }).catch((ex) => {

            console.log('ERROR RPC - getRaceDetails');
            console.log(ex);
        });
    }


    private generateEventDetailsUrl(eventId: number) {
        return this.configuration.network.host + '/ladbrokes/event/' + eventId;
    }

}
