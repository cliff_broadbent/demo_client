export interface ApiRpc {

    /**
     * Perform all steps required to retrieve a list of upcoming races and update the App State model
     */
    updateRaceList();


    /**
     * Fetch Details of a specific Race
     *
     * @param eventId
     */
    getRaceDetails(eventId: number);

}
