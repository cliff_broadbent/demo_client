import { TestBed, inject } from '@angular/core/testing';

import { HttpCommsService } from './http-comms.service';

describe('HttpCommsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpCommsService]
    });
  });

  it('should ...', inject([HttpCommsService], (service: HttpCommsService) => {
    expect(service).toBeTruthy();
  }));
});
