import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/timeout';

/**
 * Abstract comms method away from the underlying implementation
 */
@Injectable()
export class HttpCommsService {

    constructor(public http: Http) {
    }


    // Use the URL from the config service and other detail to make a httpGet request and return the response
    public httpGet(url: string, headers: Object) {

        return this.http.get(url)
            .timeout(10000)
            .toPromise();
    }

}
