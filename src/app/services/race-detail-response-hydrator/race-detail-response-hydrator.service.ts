import {Injectable} from '@angular/core';
import {RaceDetailResponseHydrator} from './race-detail-response-hydrator';

/**
 * Take raw List response/s from Ladbrokes API and normalise to our internal form.
 *
 * This class would implement the transform logic coupled to a specific API version
 */
@Injectable()
export class RaceDetailResponseHydratorService implements RaceDetailResponseHydrator {

    constructor() {
    }


    /**
     * Take the raw API response and return our internal data structure
     *
     * @param jsonData
     */
    public hydrateRaceDetails(jsonData: any) {

        let parsedResponseData = {};

        const responseData = JSON.parse(jsonData._body);

        // Sort races by time remaining
        let participants = responseData.participants;
        participants.sort(function (element1, element2) {
            let element1Key = element1.place,
                element2Key = element2.place;

            if (element1Key < element2Key) {
                return -1;
            }
            if (element1Key > element2Key) {
                return 1;
            }

            return 0;
        });

        participants = participants.filter(function () {
            return true;
        });

        parsedResponseData['lastUpdate'] = '';
        parsedResponseData['participants'] = participants;

        return parsedResponseData;
    }

}
