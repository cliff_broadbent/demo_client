import { TestBed, inject } from '@angular/core/testing';

import { RaceDetailResponseHydratorService } from './race-detail-response-hydrator.service';

describe('RaceDetailResponseHydratorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RaceDetailResponseHydratorService]
    });
  });

  it('should ...', inject([RaceDetailResponseHydratorService], (service: RaceDetailResponseHydratorService) => {
    expect(service).toBeTruthy();
  }));
});
