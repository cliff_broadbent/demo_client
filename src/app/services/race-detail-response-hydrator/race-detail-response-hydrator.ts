export interface RaceDetailResponseHydrator {

    /**
     * Take the raw API response and return our internal data structure
     *
     * @param jsonData
     */
    hydrateRaceDetails(jsonData: any)

}
