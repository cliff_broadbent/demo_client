export interface RaceListResponseHydrator {

    /**
     * Take the raw API response and return our internal data structure
     *
     * @param jsonData
     */
    hydrateRaceList(jsonData: any);

}
