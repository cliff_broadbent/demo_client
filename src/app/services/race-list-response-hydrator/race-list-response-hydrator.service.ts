import {Injectable} from '@angular/core';
import {RaceListResponseHydrator} from './race-list-response-hydrator';
import {filter} from "rxjs/operator/filter";

/**
 * Take raw List response/s from API and normalise to our internal form.
 *
 * This class would implement the transform logic coupled to a specific API version
 */
@Injectable()
export class RaceListResponseHydratorService implements RaceListResponseHydrator {

    constructor() {
    }


    /**
     * Take the raw API response and return our internal data structure
     *
     * @param jsonData
     */
    public hydrateRaceList(jsonData: any) {

        let parsedResponseData = {};

        const responseData = JSON.parse(jsonData._body);

        // Sort races by time remaining
        let races = responseData.races;
        races.sort(function (element1, element2) {
            let element1Key = new Date(element1.suspend),
                element2Key = new Date(element2.suspend);

            if (element1Key < element2Key) {
                return -1;
            }
            if (element1Key > element2Key) {
                return 1;
            }

            return 0;
        });

        races = races.filter(function(){return true;});

        parsedResponseData['races'] = races;
        parsedResponseData['apiTimeDiff'] = this.calculateTimeDiff(responseData.apiTime);

        console.log(' ...... API vs Client time diff is: ' + parsedResponseData['apiTimeDiff']);

        return parsedResponseData;
    }


    /**
     * Ensure we account for the time difference between client and server
     *
     * @param apiTime
     * @returns {string}
     */
    private calculateTimeDiff(apiTime: string) {
        const timeDiff = Date.parse(apiTime) - Date.parse(new Date().toUTCString());

        return timeDiff;
    }

}
