import { TestBed, inject } from '@angular/core/testing';

import { RaceListResponseHydratorService } from './race-list-response-hydrator.service';

describe('RaceListResponseHydratorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RaceListResponseHydratorService]
    });
  });

  it('should ...', inject([RaceListResponseHydratorService], (service: RaceListResponseHydratorService) => {
    expect(service).toBeTruthy();
  }));
});
