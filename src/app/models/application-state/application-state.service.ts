import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';


/**
 * Service that protects the data model and exposes changes as an Observable
 */
@Injectable()
export class ApplicationStateService {

    private hasUpdated = false;


    // Default Next5 app state
    private _defaultAppDataModel = {
        apiTimeDiff: '',
        races: []
    };
    // Protect model inards and expose data only via Observers
    private _appDataModel: any;
    public appDataModel: BehaviorSubject<any> = new BehaviorSubject(this._appDataModel);


    constructor() {
        this.init();
    }


    // Prime model with defaults
    public init() {
        this._appDataModel = this._defaultAppDataModel;
        this.appDataModel.next(this._appDataModel);
    }


    public getApplicationStateObservable(): BehaviorSubject<any> {
        return this.appDataModel;
    }


    /**
     * Perform an update of the Race List state
     *
     * @param eventListData
     */
    public updateRaceList(eventListData: any) {

        this.hasUpdated = true;

        this._appDataModel.races = eventListData.races;
        this.appDataModel.next(this._appDataModel);

    }


    /**
     * Attach event details to correct event in model and signal an update
     *
     * @param eventId
     * @param parsedRaceDetails
     */
    public updateRaceEventDetails(eventId: number, parsedRaceDetails: any) {
        // find the correct event
        let event = this.getRaceByEventId(eventId);

        // @todo: error check the response here

        // Attach retrieved Race Details to parent Race
        event['details'] = parsedRaceDetails;

        this.appDataModel.next(this._appDataModel);
    }


    /**
     * Fetch a specific Race by it's API provided ID
     *
     * @param eventId
     * @returns {any}
     */
    public getRaceByEventId(eventId: number) {
        for (let race of this._defaultAppDataModel.races) {
            if (race.eventId == eventId) {
                return race;
            }
        }
    }


    /**
     * Has the Race List been loaded yet?
     *
     * @returns {boolean}
     */
    public hasLoadedRaces() {
        return this.hasUpdated;
    }
}
