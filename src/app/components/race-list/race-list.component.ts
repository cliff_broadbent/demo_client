import {Component, OnInit} from '@angular/core';
import {LadbrokesApiRpcService} from '../../services/ladbrokes-api-rpc/ladbrokes-api-rpc.service';
import {ApplicationStateService} from '../../models/application-state/application-state.service';

@Component({
    selector: 'app-race-list',
    templateUrl: './race-list.component.html',
    styleUrls: ['./race-list.component.scss']
})
export class RaceListComponent implements OnInit {

    public apiTimeDiff = 0;

    public races = [];

    public loading = true;


    constructor(private rpcService: LadbrokesApiRpcService,
                private stateModel: ApplicationStateService) {

        // Listen for changes to the application state
        this.stateModel.getApplicationStateObservable().subscribe(data => {
            this.apiTimeDiff = data.apiTimeDiff;
            this.races = data.races;
            this.updateRaceCounters();
            this.loading = false;
        });
    }


    /**
     * Once view has loaded, start counters and load Race List
     */
    public ngOnInit() {

        const timeinterval = setInterval((function () {
            this.updateRaceCounters();
        }).bind(this), 500);

        // Load the race list when the application is first loaded
        if (!this.stateModel.hasLoadedRaces()) {
            this.loading = true;
            this.rpcService.updateRaceList();
        }

    }


    public updateRaceData() {
        this.races = [];
        this.loading = true;
        this.rpcService.updateRaceList();
    }


    /**
     * Update count-down timers for each Race, pruning those in which betting has closed
     */
    private updateRaceCounters() {
        for (const race of this.races) {
            race.timeRemaining = this.getTimeRemaining(race.suspend, this.apiTimeDiff);
            if (race.timeRemaining.diff <= 0) {
                // Pop the completed Race
                let arrayIndex = this.races.indexOf(race);
                if (arrayIndex > -1) {
                    this.races.splice(arrayIndex, 1);
                }
            }
        }
    }


    /**
     * Calculate the time remaining before betting closes, taking into account difference in time between API and client
     *
     * @param endtime
     * @returns {string}
     */
    private getTimeRemaining(endtime, serverTimeDiff) {
        const t = (Date.parse(endtime) - Date.parse(new Date().toUTCString())) + serverTimeDiff;
        const seconds = Math.floor((t / 1000) % 60);
        const minutes = Math.floor((t / 1000 / 60) % 60);
        const hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        const days = Math.floor(t / (1000 * 60 * 60 * 24));

        return {
            diff: t,
            days: days,
            hours: hours,
            minutes: minutes,
            seconds: seconds,
            asString: this.makePrettyTimeLeftString(days, hours, minutes, seconds)
        };
    }


    /**
     * Countdown should be clear and concise. Remove meaningless values.
     *
     * @param days
     * @param hours
     * @param minutes
     * @param seconds
     * @returns {string}
     */
    private makePrettyTimeLeftString(days, hours, minutes, seconds) {
        let prettyCounter = '';

        if (days > 0) {
            prettyCounter += days + ' days, ';
        }
        if (hours > 0) {
            prettyCounter += hours + ' hours, ';
        }
        if (minutes > 0) {
            prettyCounter += minutes + ' minutes, ';
        }
        return prettyCounter + seconds + ' seconds';
    }

}
