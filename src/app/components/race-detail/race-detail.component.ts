import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {LadbrokesApiRpcService} from '../../services/ladbrokes-api-rpc/ladbrokes-api-rpc.service';
import {ApplicationStateService} from '../../models/application-state/application-state.service';


@Component({
    selector: 'app-race-detail',
    templateUrl: './race-detail.component.html',
    styleUrls: ['./race-detail.component.scss']
})
export class RaceDetailComponent implements OnInit {

    public loading = true;

    public eventId = null;

    public eventDetails = {
        lastUpdate: '',
        participants: []
    };


    public race = {
        name: 'Loading...',
        category: ''
    };


    constructor(private rpcService: LadbrokesApiRpcService,
                private stateModel: ApplicationStateService,
                private route: ActivatedRoute,
                private router: Router) {

        // hook state service into view
        // Listen for changes to the application state
        this.stateModel.getApplicationStateObservable().subscribe(data => {

            if (this.eventId == null || !this.eventId) {
                return;
            }

            const race = this.stateModel.getRaceByEventId(this.eventId);
            // Check to avoid race condition
            if (typeof(race) !== 'undefined') {
                this.race = {
                    name: race.name,
                    category: race.category
                };

                if (race.hasOwnProperty('details')) {
                    this.eventDetails = {
                        lastUpdate: '',
                        participants: race.details.participants
                    };
                }

                this.loading = false;
            }
        });

    }

    public ngOnInit() {

        this.route.params.subscribe((params: Params) => {

            // Retrieve eventId from URL
            this.eventId = +params['eventId'];

            // @todo: this needs to be checked properly
            if (this.eventId == null || !this.eventId) {
                this.router.navigate(['/']);
                return;
            }


            let event = this.stateModel.getRaceByEventId(this.eventId);


            /**
             * We're polite when talking to people's APIs.
             *
             * Don't ask the same question constantly. Remember the response for a sensible period of time.
             */
            // @todo: this prolly needs to be tightened up with further checks
            // Does an event with this ID exist
            if (typeof event === 'undefined') {
                // make call to load events
                this.router.navigate(['/']);
                return;
            }


            // Does the returned event have attached Details
            // @todo: after this, check to see how long since we last queried the API
            if (event.hasOwnProperty('details')) {

                // There is a cached version of details we can display, no need to speak to API
                console.log('Found cached Details entry for eventId ' + this.eventId);

                this.race = {
                    name: event.name,
                    category: event.category
                };

                this.eventDetails = {
                    lastUpdate: '',
                    participants: event.details.participants
                };

                this.loading = false;

                return;
            }

            this.rpcService.getRaceDetails(this.eventId);


        });
    }


}
