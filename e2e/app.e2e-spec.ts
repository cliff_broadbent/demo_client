import { LadbrokesPage } from './app.po';

describe('ladbrokes App', () => {
  let page: LadbrokesPage;

  beforeEach(() => {
    page = new LadbrokesPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
