**What is this**
-
This is an Angular web application that displays the "Next 5 Races" and provides further detail on request.

It:

- is designed to back other APIs by changing the API-RPC service implementation;
- plays nicely with other peoples APIs, caching results for a sensible period of time where applicable.

**Prerequisites**
-
An internet connection is required for the build process.

You will need the API backend server running:

- See `https://bitbucket.org/cliff_broadbent/demo_api`

You will also require the following software installed:

- NVM https://github.com/creationix/nvm
- NodeJS v6.9+
- Git
- Angular2 CLI - `npm install -g @angular/cli`


**To use this Demo API**
-
Running this demo as follows:

- Clone the git repository to your local machine;
- Ensure you have the above prerequisites installed;
- Once the git clone has completed, navigate into the new directory;
- Execute `npm install` and wait for completion;
- Execute the `ng serve` command;
- Wait for build process to complete;
- Update `src/app/services/config/config.service.ts` if Demo API running on host other than `localhost:8010`;
- Once complete, you can access in a browser at `http://localhost:4200/`.

**If things go wrong**
-
 
Most commonly, the fault lies in either:

- Incorrect Node version;
- Incorrect Node environment variables or permissions.